const user = require("./user");

describe("user snapshot test", function() {
 it("error when snapshot - user 1", function() {
  expect(user(1)).toMatchSnapshot();
 });
 it("error when snapshot - 56", function() {
  expect(user(56)).toMatchSnapshot();
 });
 it("error when snapshot - user 1345", function() {
  expect(user(1345)).toMatchSnapshot();
    
 });
});
